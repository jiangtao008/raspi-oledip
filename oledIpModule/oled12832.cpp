#include "oled12832.h"
#include <QDebug>
#include <QString>

oled12832::oled12832():mOledHard(-1)
{
    init();
}

bool oled12832::init()
{
    wiringPiSetup();
    mOledHard = wiringPiI2CSetup(OLED_ADDR);
    if(mOledHard < 0)
    {
        qDebug()<<Q_FUNC_INFO<<"oled iic init failed!";
        return 0;
    }
    regInit(mOledHard);
    oledFill(0xff);
    showString(3,0,"......init......");
    qDebug()<<Q_FUNC_INFO<<"oled iic init sucessed! fd:"<<mOledHard;
    return 1;
}

void oled12832::regInit(int fd)
{
    writeCmd(fd,0xAE); //display off
    writeCmd(fd, 0x20);	//Set Memory Addressing Mode
    writeCmd(fd, 0x10);	//00,Horizontal Addressing Mode;01,Vertical Addressing Mode;10,Page Addressing Mode (RESET);11,Invalid
    writeCmd(fd, 0xb0);	//Set Page Start Address for Page Addressing Mode,0-7
    writeCmd(fd, 0xc8);	//Set COM Output Scan Direction
    writeCmd(fd, 0x00); //---set low column address
    writeCmd(fd, 0x10); //---set high column address
    writeCmd(fd, 0x40); //--set start line address
    writeCmd(fd, 0x81); //--set contrast control register
    writeCmd(fd, 0xff); //亮度调节 0x00~0xff
    writeCmd(fd, 0xa1); //--set segment re-map 0 to 127
    writeCmd(fd, 0xa6); //--set normal display
    writeCmd(fd, 0xa8); //--set multiplex ratio(1 to 64)
    writeCmd(fd, 0x3F); //
    writeCmd(fd, 0xa4); //0xa4,Output follows RAM content;0xa5,Output ignores RAM content
    writeCmd(fd, 0xd3); //-set display offset
    writeCmd(fd, 0x00); //-not offset
    writeCmd(fd, 0xd5); //--set display clock divide ratio/oscillator frequency
    writeCmd(fd, 0xf0); //--set divide ratio
    writeCmd(fd, 0xd9); //--set pre-charge period
    writeCmd(fd, 0x22); //
    writeCmd(fd, 0xda); //--set com pins hardware configuration
    writeCmd(fd, 0x12);
    writeCmd(fd, 0xdb); //--set vcomh
    writeCmd(fd, 0x20); //0x20,0.77xVcc
    writeCmd(fd, 0x8d); //--set DC-DC enable
    writeCmd(fd, 0x14); //
    writeCmd(fd, 0xaf); //--turn on oled panel
}

void oled12832::oledSetPos(int fd,unsigned char x, unsigned char y) //设置起始点坐标
{
    writeCmd(fd, 0xb0 + x);
    writeCmd(fd,((y & 0x0f) | 0x00));//LOW
    writeCmd(fd,(((y & 0xf0) >> 4) | 0x10));//HIGHT
}
void oled12832::writeCmd(int fd,unsigned char I2C_Command)//写命令
{
    wiringPiI2CWriteReg8(fd,0x00, I2C_Command);
}
void oled12832::writeData(int fd,unsigned char I2C_Data)//写数据
{
    wiringPiI2CWriteReg8(fd,0x40, I2C_Data);
}

void oled12832::oledFill(unsigned char data)//全屏填充
{
    for (unsigned char i = 0; i < 8; i++)
    {
        oledSetPos(mOledHard, i, 0); //设置起始点坐标
        for (int j = 0; j < 128; j++)
            writeData(mOledHard, data);//写数据
    }
}

void oled12832::oledClear()//清屏
{
    unsigned char i, j;
    for (i = 0; i < 8; i++)
    {
        oledSetPos(mOledHard, i, 0); //设置起始点坐标
        for (j = 0; j < 128; j++)
            writeData(mOledHard, 0x00);//写数据
    }
}
void oled12832::clearLine(int row)
{
    oledSetPos(mOledHard,row*2, 0); //设置起始点坐标
    for (int j = 0; j < 128; j++)
        writeData(mOledHard, 0x00);//写数据
    oledSetPos(mOledHard,row*2+1, 0); //设置起始点坐标
    for (int j = 0; j < 128; j++)
        writeData(mOledHard, 0x00);//写数据
}
//显示字符
void oled12832::showASCLL(unsigned char row, unsigned char col, unsigned char ascii_char)
{
    unsigned char  i,j;
    i = ascii_char - ' ';	//获取字符偏移量，这是因为字库跟标准ASCII码表相差32，即一个空格
    writeCmd(mOledHard ,0xb0+row);	//设置页地址
    writeCmd(mOledHard ,col&0x0F);			//设置列地址
    writeCmd(mOledHard ,((col&0xF0)>>4)|0x10);

    for(j=0;j<8;j++)
        writeData(mOledHard ,oled_fonts1608[i][j]);

    writeCmd(mOledHard ,0xB0+(row&0x07)+1);	//设置下一页地址
    writeCmd(mOledHard ,col&0x0F);			//设置列地址
    writeCmd(mOledHard ,((col&0xF0)>>4)|0x10);
    for(j=0;j<8;j++)
        writeData(mOledHard ,oled_fonts1608[i][j+8]);
}

//显示字符串
void oled12832::showString(unsigned char row, unsigned char col, unsigned char *ascii_string)
{
    row = row*2;
    while(*ascii_string != '\0')
    {
        if(col == 128)	//防止出现长度大于16的字符串在同一行显示的情况
        {
            col = 0;
            row += 2;
        }
        showASCLL(row, col, *ascii_string);
        col += 8;
        ascii_string++;
    }
}

void oled12832::showString(unsigned char row, unsigned char col, QString str)
{
    QByteArray buff = str.toLatin1();
    unsigned char *ptr = (unsigned char *)buff.data();
    showString(row,col,ptr);
}




