#ifndef OLEDIP_H
#define OLEDIP_H

#include <QObject>
#include <QTimer>
#include <QTime>
#include "oled12832.h"

class OledIP : public QObject
{
    Q_OBJECT
public:
    explicit OledIP(QObject *parent = nullptr);

    QString getLocalIp();
    QString getMAC();
    QString getCpuTemp();
signals:

public slots:

private:
    oled12832 oled;
    QTimer *updataTime;
};

#endif // OLEDIP_H
