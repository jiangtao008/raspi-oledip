#include <QCoreApplication>
#include "oledip.h"

pid_t getProcessPidByName(const char *proc_name)
{
     FILE *fp;
     char buf[100];
     char cmd[200] = {'\0'};
     pid_t pid = -1;
     sprintf(cmd, "pidof %s", proc_name);
     if((fp = popen(cmd, "r")) != NULL)
     {
         if(fgets(buf, 255, fp) != NULL)
             pid = atoi(buf);
     }
     pclose(fp);
     return pid;
}

#include <stdio.h>

#include <unistd.h>
#include <fcntl.h>
#include <errno.h>

int main(int argc, char *argv[])
{

    int lock_result;
    char * pFileName = "oledIP.lck";
    int fd = open(pFileName,O_RDWR);
    if(fd>=0)
    {

        lock_result = lockf(fd,F_TEST,0);  //参数使用F_LOCK，则如果已经加锁，则阻塞到前一个进程释放锁为止，参数0表示对整个文件加锁
        if(lock_result < 0)//返回0表示未加锁或者被当前进程加锁；返回-1表示被其他进程加锁
        {
            perror("Exec lockf function failed.\n");
            return 1;
        }

        //参数使用F_LOCK，则如果已经加锁，则阻塞到前一个进程释放锁为止，参数0表示对整个文件加锁
        lock_result = lockf(fd,F_LOCK,0);
        if(lock_result<0)
        {
            perror("Exec lockf function failed.\n");
            return 1;
        }
    }

    QCoreApplication a(argc, argv);
    OledIP oledIP;
    return a.exec();
}


